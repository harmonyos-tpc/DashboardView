DashboardView
===============

## Introduction

** Dashboard, Imitation Sesame credit (old and new), Cool car Speed Gauge**
Dashboard helps to portray range graphically, so it will be more communicative. here we will learn custom dashboard controls. As  there are too many variable attributes of the dashboard control, building single dashboard using attribute configs may not meet broad requirements. Therefore, different dashboard Style is implemented. If a dashboard Style is suitable for the project, you can directly modify the dashboard attributes.

##Screenshot
###Dashboard Style1
![](screenshot/style1.png)
###Dashboard Style2 & Dashboard Style3

![](screenshot/style2.png)  ![](screenshot/style3.png)

###Dashboard Style4
![](screenshot/style4.png)

## Usage Instructions
Choose the suitable Dashboard style and directly modify its attributes as per the requirement.

DashboardView styles are implemented using components, in resource file it can be included as below.

```
 <com.xw.sample.dashboardviewdemo.DashboardView1
            ohos:id="$+id:dashboard_view_1"
            ohos:height="match_content"
            ohos:width="match_content"/>
````

## Installation Instructions

Choose the suitable Dashboard style, copy the dashboard file to your application and directly modify its attributes as per the requirement.



##License
```
The MIT License (MIT)

Copyright (c) 2016 woxingxiao

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
