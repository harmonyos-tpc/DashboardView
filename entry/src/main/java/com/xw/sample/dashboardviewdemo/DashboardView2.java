package com.xw.sample.dashboardviewdemo;

import com.xw.sample.dashboardviewdemo.util.ArgbEvaluator;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.RadialShader;
import ohos.agp.render.Shader;
import ohos.agp.render.SweepShader;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.global.configuration.DeviceCapability;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static ohos.agp.components.Component.EstimateSpec.getSizeWithMode;
import static ohos.global.resource.solidxml.TypedAttribute.computeTranslateRatio;

/**
 * DashboardView style 2 仿芝麻信用分
 * Created by woxingxiao on 2016-11-19.
 */

public class DashboardView2 extends Component implements Component.DrawTask, Component.EstimateSizeListener {
    private int mRadius; // 画布边缘半径（去除padding后的半径）
    private int mStartAngle = 150; // 起始角度
    private int mSweepAngle = 240; // 绘制角度
    private int mMin = 350; // 最小值
    private int mMax = 950; // 最大值
    private int mSection = 10; // 值域（mMax-mMin）等分份数
    private int mPortion = 3; // 一个mSection等分份数
    private String mHeaderText = "BETA"; // 表头
    private int mCreditValue = 650; // 信用分
    private int mSolidCreditValue = mCreditValue; // 信用分(设定后不变)
    private int mSparkleWidth; // 亮点宽度
    private int mProgressWidth; // 进度圆弧宽度
    private float mLength1; // 刻度顶部相对边缘的长度
    private int mCalibrationWidth; // 刻度圆弧宽度
    private float mLength2; // 刻度读数顶部相对边缘的长度

    private int mPadding;
    private float mCenterX, mCenterY; // 圆心坐标
    private Paint mPaint;
    private RectFloat mRectFProgressArc;
    private RectFloat mRectFCalibrationFArc;
    private RectFloat mRectFTextArc;
    private Path mPath;
    private Rect mRectText;
    private String[] mTexts;
    private int mBackgroundColor;
    private int[] mBgColors;
    private ArgbEvaluator argbEvaluator = new ArgbEvaluator();

    /**
     * 由于真实的芝麻信用界面信用值不是线性排布，所以播放动画时若以信用值为参考，则会出现忽慢忽快
     * 的情况（开始以为是卡顿）。因此，先计算出最终到达角度，以扫过的角度为线性参考，动画就流畅了
     */
    private boolean isAnimFinish = true;
    private float mAngleWhenAnim;

    public DashboardView2(Context context) {
        this(context, null);
    }

    public DashboardView2(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    public DashboardView2(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        addDrawTask(this);
        setEstimateSizeListener(this);
        init();
    }

    private void init() {
        mSparkleWidth = dp2px(10);
        mProgressWidth = dp2px(3);
        mCalibrationWidth = dp2px(10);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);

        mRectFProgressArc = new RectFloat();
        mRectFCalibrationFArc = new RectFloat();
        mRectFTextArc = new RectFloat();
        mPath = new Path();
        mRectText = new Rect();

        mTexts = new String[]{"350", "Poor", "550", "Med", "600", "Good", "650", "Excel", "700", "Excel", "950"};
        mBgColors = new int[]{getContext().getColor(ResourceTable.Color_color_red),
                getContext().getColor(ResourceTable.Color_color_orange),
                getContext().getColor(ResourceTable.Color_color_yellow),
                getContext().getColor(ResourceTable.Color_color_green),
                getContext().getColor(ResourceTable.Color_color_blue)};
        mBackgroundColor = mBgColors[0];
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        mPadding = Math.max(
                Math.max(getPaddingLeft(), getPaddingTop()),
                Math.max(getPaddingRight(), getPaddingBottom())
        );
        setPadding(mPadding, mPadding, mPadding, mPadding);

        mLength1 = mPadding + mSparkleWidth / 2f + dp2px(8);
        mLength2 = mLength1 + mCalibrationWidth + dp2px(1) + dp2px(5);

        int estwidth = getSizeWithMode(dp2px(220), EstimateSpec.getMode(widthEstimateConfig));
        int width = EstimateSpec.getSize(estwidth);

        mRadius = (width - mPadding * 2) / 2;

        setEstimatedSize(estwidth, EstimateSpec.getSizeWithMode(width - dp2px(30), EstimateSpec.getMode(widthEstimateConfig)));
        mCenterX = mCenterY = getEstimatedWidth() / 2f;
        mRectFProgressArc = new RectFloat(
                mPadding + mSparkleWidth / 2f,
                mPadding + mSparkleWidth / 2f,
                getEstimatedWidth() - mPadding - mSparkleWidth / 2f,
                getEstimatedWidth() - mPadding - mSparkleWidth / 2f
        );

        mRectFCalibrationFArc = new RectFloat(
                mLength1 + mCalibrationWidth / 2f,
                mLength1 + mCalibrationWidth / 2f,
                getEstimatedWidth() - mLength1 - mCalibrationWidth / 2f,
                getEstimatedWidth() - mLength1 - mCalibrationWidth / 2f
        );

        mPaint.setTextSize(sp2px(10));
        mRectText = mPaint.getTextBounds("0");
        mRectFTextArc = new RectFloat(
                mLength2 + mRectText.getHeight(),
                mLength2 + mRectText.getHeight(),
                getEstimatedWidth() - mLength2 - mRectText.getHeight(),
                getEstimatedWidth() - mLength2 - mRectText.getHeight()
        );
        return true;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {

        canvas.drawColor(mBackgroundColor, Canvas.PorterDuffMode.SRC_OVER);

        /**
         * 画进度圆弧背景
         */
        mPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setStrokeWidth(mProgressWidth);
        mPaint.setAlpha(80 / 255.0f);
        canvas.drawArc(mRectFProgressArc, new Arc(mStartAngle + 1, mSweepAngle - 2, false), mPaint);

        mPaint.setAlpha(1.0f);
        if (isAnimFinish) {
            /**
             * 画进度圆弧(起始到信用值)
             */
            mPaint.setShader(generateSweepGradient(), Paint.ShaderType.SWEEP_SHADER);
            canvas.drawArc(mRectFProgressArc, new Arc(mStartAngle + 1, calculateRelativeAngleWithValue(mCreditValue) - 2, false), mPaint);
            /**
             * 画信用值指示亮点
             */
            float[] point = getCoordinatePoint(
                    mRadius - mSparkleWidth / 2f,
                    mStartAngle + calculateRelativeAngleWithValue(mCreditValue)
            );
            mPaint.setStyle(Paint.Style.FILL_STYLE);
            mPaint.setShader(generateRadialGradient(point[0], point[1]), Paint.ShaderType.RADIAL_SHADER);
            canvas.drawCircle(point[0], point[1], mSparkleWidth / 2f, mPaint);
        } else {
            /**
             * 画进度圆弧(起始到信用值)
             */
            mPaint.setShader(generateSweepGradient(), Paint.ShaderType.SWEEP_SHADER);
            canvas.drawArc(mRectFProgressArc, new Arc(mStartAngle + 1, mAngleWhenAnim - mStartAngle - 2, false), mPaint);
            /**
             * 画信用值指示亮点
             */
            float[] point = getCoordinatePoint(
                    mRadius - mSparkleWidth / 2f,
                    mAngleWhenAnim
            );
            mPaint.setStyle(Paint.Style.FILL_STYLE);
            mPaint.setShader(generateRadialGradient(point[0], point[1]), Paint.ShaderType.RADIAL_SHADER);
            canvas.drawCircle(point[0], point[1], mSparkleWidth / 2f, mPaint);
        }

        /**
         * 画刻度圆弧
         */
        mPaint.setShader(null, Paint.ShaderType.LINEAR_SHADER);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setColor(Color.WHITE);
        mPaint.setAlpha(80 / 255.0f);
        mPaint.setStrokeCap(Paint.StrokeCap.SQUARE_CAP);
        mPaint.setStrokeWidth(mCalibrationWidth);
        canvas.drawArc(mRectFCalibrationFArc, new Arc(mStartAngle + 3, mSweepAngle - 6, false), mPaint);

        /**
         * 画长刻度
         * 画好起始角度的一条刻度后通过canvas绕着原点旋转来画剩下的长刻度
         */
        mPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        mPaint.setStrokeWidth(dp2px(2));
        mPaint.setAlpha(120 / 255.0f);
        float x0 = mCenterX;
        float y0 = mPadding + mLength1 + dp2px(1);
        float x1 = mCenterX;
        float y1 = y0 + mCalibrationWidth;
        // 逆时针到开始处
        canvas.save();
        canvas.drawLine(x0, y0, x1, y1, mPaint);
        float degree = mSweepAngle / mSection;
        for (int i = 0; i < mSection / 2; i++) {
            canvas.rotate(-degree, mCenterX, mCenterY);
            canvas.drawLine(x0, y0, x1, y1, mPaint);
        }
        canvas.restore();
        // 顺时针到结尾处
        canvas.save();
        for (int i = 0; i < mSection / 2; i++) {
            canvas.rotate(degree, mCenterX, mCenterY);
            canvas.drawLine(x0, y0, x1, y1, mPaint);
        }
        canvas.restore();

        /**
         * 画短刻度
         * 同样采用canvas的旋转原理
         */
        mPaint.setStrokeWidth(dp2px(1));
        mPaint.setAlpha(80 / 255.0f);
        float x2 = mCenterX;
        float y2 = y0 + mCalibrationWidth - dp2px(2);
        // 逆时针到开始处
        canvas.save();
        canvas.drawLine(x0, y0, x2, y2, mPaint);
        degree = mSweepAngle / (mSection * mPortion);
        for (int i = 0; i < (mSection * mPortion) / 2; i++) {
            canvas.rotate(-degree, mCenterX, mCenterY);
            canvas.drawLine(x0, y0, x2, y2, mPaint);
        }
        canvas.restore();
        // 顺时针到结尾处
        canvas.save();
        for (int i = 0; i < (mSection * mPortion) / 2; i++) {
            canvas.rotate(degree, mCenterX, mCenterY);
            canvas.drawLine(x0, y0, x2, y2, mPaint);
        }
        canvas.restore();

        /**
         * 画长刻度读数
         * 添加一个圆弧path，文字沿着path绘制
         */
        mPaint.setTextSize(sp2px(10));
        mPaint.setTextAlign(TextAlignment.LEFT);
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setAlpha(160 / 255.0f);
        for (int i = 0; i < mTexts.length; i++) {
            mRectText = mPaint.getTextBounds(mTexts[i]);
            // 粗略把文字的宽度视为圆心角2*θ对应的弧长，利用弧长公式得到θ，下面用于修正角度
            float θ = (float) (180 * mRectText.getWidth() / 2 /
                    (Math.PI * (mRadius - mLength2 - mRectText.getHeight())));

            mPath.reset();
            mPath.addArc(
                    mRectFTextArc,
                    mStartAngle + i * (mSweepAngle / mSection) - θ, // 正起始角度减去θ使文字居中对准长刻度
                    mSweepAngle
            );
            canvas.drawTextOnPath(mPaint, mTexts[i], mPath, 0, 0);
        }

        /**
         * 画实时度数值
         */
        mPaint.setAlpha(1.0f);
        mPaint.setTextSize(sp2px(50));
        mPaint.setTextAlign(TextAlignment.CENTER);
        String value = String.valueOf(mSolidCreditValue);
        canvas.drawText(mPaint, value, mCenterX, mCenterY + dp2px(30));

        /**
         * 画表头
         */
        mPaint.setAlpha(160 / 255.0f);
        mPaint.setTextSize(sp2px(12));
        canvas.drawText(mPaint, mHeaderText, mCenterX, mCenterY - dp2px(20));

        /**
         * 画信用描述
         */
        mPaint.setAlpha(1.0f);
        mPaint.setTextSize(sp2px(20));
        canvas.drawText(mPaint, calculateCreditDescription(), mCenterX, mCenterY + dp2px(55));

        /**
         * 画评估时间
         */
        mPaint.setAlpha(160 / 255.0f);
        mPaint.setTextSize(sp2px(10));
        canvas.drawText(mPaint, getFormatTimeStr(), mCenterX, mCenterY + dp2px(70));
    }

    private int dp2px(int dp) {
        return (int) (dp * computeTranslateRatio(new DeviceCapability()));
    }

    private int sp2px(int sp) {
        return (int) (sp * computeTranslateRatio(new DeviceCapability()));
    }

    private SweepShader generateSweepGradient() {
        SweepShader sweepGradient = new SweepShader(mCenterX, mCenterY,
                new Color[]{new Color(Color.argb(0, 255, 255, 255)), new Color(Color.argb(200, 255, 255, 255))},
                new float[]{0, calculateRelativeAngleWithValue(mCreditValue) / 360}
        );
        Matrix matrix = new Matrix();
        matrix.setRotate(mStartAngle - 1, mCenterX, mCenterY);
        sweepGradient.setShaderMatrix(matrix);

        return sweepGradient;
    }

    private RadialShader generateRadialGradient(float x, float y) {
        return new RadialShader(new Point(x, y), mSparkleWidth / 2f,
                new float[]{0.4f, 1},
                new Color[]{new Color(Color.argb(255, 255, 255, 255)), new Color(Color.argb(80, 255, 255, 255))},
                Shader.TileMode.CLAMP_TILEMODE
        );
    }

    private float[] getCoordinatePoint(float radius, float angle) {
        float[] point = new float[2];

        double arcAngle = Math.toRadians(angle); //将角度转换为弧度
        if (angle < 90) {
            point[0] = (float) (mCenterX + Math.cos(arcAngle) * radius);
            point[1] = (float) (mCenterY + Math.sin(arcAngle) * radius);
        } else if (angle == 90) {
            point[0] = mCenterX;
            point[1] = mCenterY + radius;
        } else if (angle > 90 && angle < 180) {
            arcAngle = Math.PI * (180 - angle) / 180.0;
            point[0] = (float) (mCenterX - Math.cos(arcAngle) * radius);
            point[1] = (float) (mCenterY + Math.sin(arcAngle) * radius);
        } else if (angle == 180) {
            point[0] = mCenterX - radius;
            point[1] = mCenterY;
        } else if (angle > 180 && angle < 270) {
            arcAngle = Math.PI * (angle - 180) / 180.0;
            point[0] = (float) (mCenterX - Math.cos(arcAngle) * radius);
            point[1] = (float) (mCenterY - Math.sin(arcAngle) * radius);
        } else if (angle == 270) {
            point[0] = mCenterX;
            point[1] = mCenterY - radius;
        } else {
            arcAngle = Math.PI * (360 - angle) / 180.0;
            point[0] = (float) (mCenterX + Math.cos(arcAngle) * radius);
            point[1] = (float) (mCenterY - Math.sin(arcAngle) * radius);
        }

        return point;
    }

    /**
     * 相对起始角度计算信用分所对应的角度大小
     * @param value value
     * @return calculate and returrn relative angle
     */
    private float calculateRelativeAngleWithValue(int value) {
        float degreePerSection = 1f * mSweepAngle / mSection;
        if (value > 700) {
            return 8 * degreePerSection + 2 * degreePerSection / 250 * (value - 700);
        } else if (value > 650) {
            return 6 * degreePerSection + 2 * degreePerSection / 50 * (value - 650);
        } else if (value > 600) {
            return 4 * degreePerSection + 2 * degreePerSection / 50 * (value - 600);
        } else if (value > 550) {
            return 2 * degreePerSection + 2 * degreePerSection / 50 * (value - 550);
        } else {
            return 2 * degreePerSection / 200 * (value - 350);
        }
    }

    /**
     * 信用分对应信用描述
     * @return grade based on credit score
     */
    private String calculateCreditDescription() {
        if (mSolidCreditValue > 700) {
            return "Excel Credit";
        } else if (mSolidCreditValue > 650) {
            return "Excel Credit";
        } else if (mSolidCreditValue > 600) {
            return "Good Credit";
        } else if (mSolidCreditValue > 550) {
            return "Avg Credit";
        }
        return "Poor Credit";
    }

    private SimpleDateFormat mDateFormat;

    private String getFormatTimeStr() {
        if (mDateFormat == null) {
            mDateFormat = new SimpleDateFormat("yyyy.MM.dd", Locale.CHINA);
        }
        return String.format("EvalTime:%s", mDateFormat.format(new Date()));
    }

    public int getCreditValue() {
        return mCreditValue;
    }

    /**
     * 设置信用值
     *
     * @param creditValue 信用值
     */
    public void setCreditValue(int creditValue) {
        if (mSolidCreditValue == creditValue || creditValue < mMin || creditValue > mMax) {
            return;
        }

        mSolidCreditValue = creditValue;
        mCreditValue = creditValue;
        invalidate();
    }

    /**
     * 设置信用值并播放动画
     *
     * @param creditValue 信用值
     */
    public void setCreditValueWithAnim(int creditValue) {
        if (creditValue < mMin || creditValue > mMax || !isAnimFinish) {
            return;
        }

        mSolidCreditValue = creditValue;

        AnimatorValue creditValueAnimator = new AnimatorValue();
        creditValueAnimator.setValueUpdateListener((animatorValue, v) -> {
            mCreditValue = 350 + (int) ((mSolidCreditValue - 350) * v);
            invalidate();
        });

        // 计算最终值对应的角度，以扫过的角度的线性变化来播放动画
        float degree = calculateRelativeAngleWithValue(mSolidCreditValue);

        AnimatorValue degreeValueAnimator = new AnimatorValue();
        degreeValueAnimator.setValueUpdateListener((animatorValue, v) -> mAngleWhenAnim = mStartAngle + degree * v);

        AnimatorValue colorAnimator = new AnimatorValue();
        // 实时信用值对应的背景色的变化
        long delay = 1000;
        if (mSolidCreditValue > 700) {
            delay = 3000;
        } else if (mSolidCreditValue > 650) {
            delay = 2500;
        } else if (mSolidCreditValue > 600) {
            delay = 2000;
        } else if (mSolidCreditValue > 550) {
            delay = 1500;
        }
        colorAnimator.setValueUpdateListener((animatorValue, v) -> mBackgroundColor = getColorValue(v));

        AnimatorGroup animatorSet = new AnimatorGroup();
        animatorSet.setDuration(delay);
        animatorSet.runParallel(creditValueAnimator, degreeValueAnimator, colorAnimator);
        animatorSet.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                isAnimFinish = false;
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {
                isAnimFinish = true;
            }

            @Override
            public void onEnd(Animator animator) {
                isAnimFinish = true;
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        animatorSet.start();
    }

    private int getColorValue(float v) {
        int[] bgcolors;
        if (mSolidCreditValue > 700) {
            bgcolors = new int[]{mBgColors[0], mBgColors[1], mBgColors[2], mBgColors[3], mBgColors[4]};
        } else if (mSolidCreditValue > 650) {
            bgcolors = new int[]{mBgColors[0], mBgColors[1], mBgColors[2], mBgColors[3]};
        } else if (mSolidCreditValue > 600) {
            bgcolors = new int[]{mBgColors[0], mBgColors[1], mBgColors[2]};
        } else if (mSolidCreditValue > 550) {
            bgcolors = new int[]{mBgColors[0], mBgColors[1]};
        } else {
            bgcolors = new int[]{mBgColors[0], mBgColors[1]};
        }

        int range;
        if (v > .999)
            range = bgcolors.length - 2;
        else {
            range = ((int) (v / (1.0f / bgcolors.length))) - 1;
        }
        if (range == -1)
            range = 0;
        return (int) ArgbEvaluator.getInstance().evaluate(v, bgcolors[range], bgcolors[range + 1]);
    }
}
