/*
 * Copyright (c) 2021-2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xw.sample.dashboardviewdemo.slice;

import com.xw.sample.dashboardviewdemo.DashboardView1;
import com.xw.sample.dashboardviewdemo.DashboardView2;
import com.xw.sample.dashboardviewdemo.DashboardView3;
import com.xw.sample.dashboardviewdemo.DashboardView4;
import com.xw.sample.dashboardviewdemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;

import java.util.Random;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private DashboardView1 mDashboardView1;
    private DashboardView2 mDashboardView2;
    private DashboardView3 mDashboardView3;
    private DashboardView4 mDashboardView4;
    private boolean isAnimFinished = true;
    private int mVel;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        mDashboardView1 = (DashboardView1) findComponentById(ResourceTable.Id_dashboard_view_1);
        mDashboardView2 = (DashboardView2) findComponentById(ResourceTable.Id_dashboard_view_2);
        mDashboardView3 = (DashboardView3) findComponentById(ResourceTable.Id_dashboard_view_3);
        mDashboardView4 = (DashboardView4) findComponentById(ResourceTable.Id_dashboard_view_4);

        mDashboardView1.setClickedListener(this);
        mDashboardView2.setClickedListener(this);
        mDashboardView3.setClickedListener(this);
        mDashboardView4.setClickedListener(this);

        mDashboardView2.setCreditValueWithAnim(new Random().nextInt(600) + 350);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_dashboard_view_1:
                mDashboardView1.setRealTimeValue(new Random().nextInt(100));

                break;
            case ResourceTable.Id_dashboard_view_2:
                mDashboardView2.setCreditValueWithAnim(new Random().nextInt(950 - 350) + 350);

                break;
            case ResourceTable.Id_dashboard_view_3:
                mDashboardView3.setCreditValue(new Random().nextInt(950 - 350) + 350);

                break;
            case ResourceTable.Id_dashboard_view_4:
                if (isAnimFinished) {
                    AnimatorValue animator = new AnimatorValue();
                    mVel = new Random().nextInt(180);
                    animator.setDuration(1500);
                    animator.setCurveType(Animator.CurveType.LINEAR);
                    animator.setStateChangedListener(new Animator.StateChangedListener() {
                        @Override
                        public void onStart(Animator animator) {
                            isAnimFinished = false;
                        }

                        @Override
                        public void onStop(Animator animator) {

                        }

                        @Override
                        public void onCancel(Animator animator) {
                            isAnimFinished = true;
                        }

                        @Override
                        public void onEnd(Animator animator) {
                            isAnimFinished = true;
                        }

                        @Override
                        public void onPause(Animator animator) {

                        }

                        @Override
                        public void onResume(Animator animator) {

                        }
                    });
                    animator.setValueUpdateListener((animatorValue, v) -> mDashboardView4.setVelocity((int) (mVel * v)));
                    animator.start();
                }
                break;
        }
    }
}
