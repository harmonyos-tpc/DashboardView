package com.xw.sample.dashboardviewdemo;

import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.RadialShader;
import ohos.agp.render.Shader;
import ohos.agp.render.SweepShader;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.global.configuration.DeviceCapability;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static ohos.agp.components.Component.EstimateSpec.getSizeWithMode;
import static ohos.global.resource.solidxml.TypedAttribute.computeTranslateRatio;

/**
 * DashboardView style 3 仿最新版芝麻信用分
 * Created by woxingxiao on 2016-11-28.
 */

public class DashboardView3 extends Component implements Component.DrawTask, Component.EstimateSizeListener {

    private int mRadius; // 画布边缘半径（去除padding后的半径）
    private int mStartAngle = 150; // 起始角度
    private int mSweepAngle = 240; // 绘制角度
    private int mMin = 350; // 最小值
    private int mMax = 950; // 最大值
    private String mHeaderText = "BETA"; // 表头
    private int mCreditValue = 782; // 信用分
    private int mSparkleWidth; // 亮点宽度
    private int mProgressWidth; // 进度圆弧宽度
    private float mLength1; // 刻度顶部相对边缘的长度
    private float mLength2; // 信用值指示器顶部相对边缘的长度

    private int mPadding;
    private float mCenterX, mCenterY; // 圆心坐标
    private Paint mPaint;
    private RectFloat mRectFProgressArc;
    private Rect mRectText;
    private Path mPath;
    private int[] mBgColors;

    public DashboardView3(Context context) {
        this(context, null);
    }

    public DashboardView3(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    public DashboardView3(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        addDrawTask(this);
        setEstimateSizeListener(this);
        init();
    }

    private void init() {
        mSparkleWidth = dp2px(10);
        mProgressWidth = 4;

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStrokeCap(Paint.StrokeCap.SQUARE_CAP);
        mPaint.setColor(Color.WHITE);

        mRectFProgressArc = new RectFloat();
        mRectText = new Rect();
        mPath = new Path();

        mBgColors = new int[]{getContext().getColor(ResourceTable.Color_color_red),
                getContext().getColor(ResourceTable.Color_color_orange),
                getContext().getColor(ResourceTable.Color_color_yellow),
                getContext().getColor(ResourceTable.Color_color_green),
                getContext().getColor(ResourceTable.Color_color_blue)};
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {

        mPadding = Math.max(
                Math.max(getPaddingLeft(), getPaddingTop()),
                Math.max(getPaddingRight(), getPaddingBottom())
        );
        setPadding(mPadding, mPadding, mPadding, mPadding);

        mLength1 = mPadding + mSparkleWidth / 2f + dp2px(8);
        mLength2 = mLength1 + mProgressWidth + dp2px(4);
        int estwidth = getSizeWithMode(dp2px(220), EstimateSpec.getMode(widthEstimateConfig));
        int width = EstimateSpec.getSize(estwidth);

        mRadius = (width - mPadding * 2) / 2;
        setEstimatedSize(estwidth, EstimateSpec.getSizeWithMode(width - dp2px(50), EstimateSpec.getMode(widthEstimateConfig)));

        mCenterX = mCenterY = getEstimatedWidth() / 2f;
        mRectFProgressArc = new RectFloat(
                mPadding + mSparkleWidth / 2f,
                mPadding + mSparkleWidth / 2f,
                getEstimatedWidth() - mPadding - mSparkleWidth / 2f,
                getEstimatedWidth() - mPadding - mSparkleWidth / 2f
        );

        mPaint.setTextSize(sp2px(10));
        mRectText = mPaint.getTextBounds("0");
        return true;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.drawColor(calculateBGColorWithValue(mCreditValue), Canvas.PorterDuffMode.SRC_OVER);

        /**
         * 画进度圆弧背景
         */
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setStrokeWidth(mProgressWidth);
        mPaint.setAlpha(80/255.0f);
        canvas.drawArc(mRectFProgressArc, new Arc(mStartAngle, mSweepAngle, false), mPaint);

        mPaint.setAlpha(1.0f);
        /**
         * 画进度圆弧(起始到信用值)
         */
        mPaint.setShader(generateSweepGradient(), Paint.ShaderType.SWEEP_SHADER);
        canvas.drawArc(mRectFProgressArc, new Arc(mStartAngle, calculateRelativeAngleWithValue(mCreditValue), false), mPaint);
        /**
         * 画信用值指示亮点
         */
        float[] point = getCoordinatePoint(
                mRadius - mSparkleWidth / 2f,
                mStartAngle + calculateRelativeAngleWithValue(mCreditValue)
        );
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setShader(generateRadialGradient(point[0], point[1]), Paint.ShaderType.RADIAL_SHADER);
        canvas.drawCircle(point[0], point[1], mSparkleWidth / 2f, mPaint);

        /**
         * 画刻度
         */
        int cnt = (mMax - mMin) / 2 / 10;
        float degree = mSweepAngle / ((mMax - mMin) / 10);
        float a = calculateRelativeAngleWithValue(mCreditValue);
        float b = mSweepAngle / 2f;
        mPaint.setShader(null, Paint.ShaderType.LINEAR_SHADER);
        mPaint.setAlpha(a >= b ? 200/255.0f : 80/255.0f);
        canvas.save();
        canvas.drawLine(mCenterX, mPadding + mLength1, mCenterX, mPadding + mLength1 - 1, mPaint);
        // 逆时针旋转
        for (int i = 0; i < cnt; i++) {
            canvas.rotate(-degree, mCenterX, mCenterY);
            b -= degree;
            mPaint.setAlpha(a >= b ? 200/255.0f : 80/255.0f);
            canvas.drawLine(mCenterX, mPadding + mLength1, mCenterX, mPadding + mLength1 - 1, mPaint);
        }
        canvas.restore();
        canvas.save();
        // 顺时针旋转
        b = mSweepAngle / 2f;
        for (int i = 0; i < cnt; i++) {
            canvas.rotate(degree, mCenterX, mCenterY);
            b += degree;
            mPaint.setAlpha(a >= b ? 200/255.0f : 80/255.0f);
            canvas.drawLine(mCenterX, mPadding + mLength1, mCenterX, mPadding + mLength1 - 1, mPaint);
        }
        canvas.restore();

        /**
         * 画信用分指示器
         */
        canvas.save();
        b = mSweepAngle / 2f;
        canvas.rotate(a - b, mCenterX, mCenterY);
        mPaint.setAlpha(1.0f);
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPath.reset();
        mPath.moveTo(mCenterX, mPadding + mLength2);
        mPath.rLineTo(-dp2px(2), dp2px(5));
        mPath.rLineTo(dp2px(4), 0);
        mPath.close();
        canvas.drawPath(mPath, mPaint);
        mPaint.setStrokeWidth(dp2px(1));
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        canvas.drawCircle(mCenterX, mPadding + mLength2 + dp2px(6) + 1, dp2px(2), mPaint);
        canvas.restore();

        /**
         * 画实时度数值
         */
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setTextSize(sp2px(35));
        mPaint.setTextAlign(TextAlignment.CENTER);
        String value = String.valueOf(mCreditValue);
        canvas.drawText(mPaint, value, mCenterX, mCenterY);

        /**
         * 画信用描述
         */
        mPaint.setTextSize(sp2px(14));
        canvas.drawText(mPaint, calculateCreditDescription(), mCenterX, mCenterY - dp2px(40));

        /**
         * 画表头
         */
        mPaint.setAlpha(160/255.0f);
        mPaint.setTextSize(sp2px(10));
        canvas.drawText(mPaint, mHeaderText, mCenterX, mCenterY - dp2px(65));

        /**
         * 画评估时间
         */
        mPaint.setAlpha(160/255.0f);
        mPaint.setTextSize(sp2px(9));
        canvas.drawText(mPaint, getFormatTimeStr(), mCenterX, mCenterY + dp2px(25));
    }

    private int dp2px(int dp) {
        return (int) (dp * computeTranslateRatio(new DeviceCapability()));
    }

    private int sp2px(int sp) {
        return (int) (sp * computeTranslateRatio(new DeviceCapability()));
    }

    private SweepShader generateSweepGradient() {
        SweepShader sweepGradient = new SweepShader(mCenterX, mCenterY,
                new Color[]{new Color(Color.argb(0, 255, 255, 255)), new Color(Color.argb(200, 255, 255, 255))},
                new float[]{0, calculateRelativeAngleWithValue(mCreditValue) / 360}
        );
        Matrix matrix = new Matrix();
        matrix.setRotate(mStartAngle - 1, mCenterX, mCenterY);
        sweepGradient.setShaderMatrix(matrix);

        return sweepGradient;
    }

    private RadialShader generateRadialGradient(float x, float y) {
        return new RadialShader(new Point(x, y), mSparkleWidth / 2f,
                new float[]{0.4f, 1},
                new Color[]{new Color(Color.argb(255, 255, 255, 255)), new Color(Color.argb(80, 255, 255, 255))},
                Shader.TileMode.CLAMP_TILEMODE
        );
    }

    private float[] getCoordinatePoint(float radius, float angle) {
        float[] point = new float[2];

        double arcAngle = Math.toRadians(angle); //将角度转换为弧度
        if (angle < 90) {
            point[0] = (float) (mCenterX + Math.cos(arcAngle) * radius);
            point[1] = (float) (mCenterY + Math.sin(arcAngle) * radius);
        } else if (angle == 90) {
            point[0] = mCenterX;
            point[1] = mCenterY + radius;
        } else if (angle > 90 && angle < 180) {
            arcAngle = Math.PI * (180 - angle) / 180.0;
            point[0] = (float) (mCenterX - Math.cos(arcAngle) * radius);
            point[1] = (float) (mCenterY + Math.sin(arcAngle) * radius);
        } else if (angle == 180) {
            point[0] = mCenterX - radius;
            point[1] = mCenterY;
        } else if (angle > 180 && angle < 270) {
            arcAngle = Math.PI * (angle - 180) / 180.0;
            point[0] = (float) (mCenterX - Math.cos(arcAngle) * radius);
            point[1] = (float) (mCenterY - Math.sin(arcAngle) * radius);
        } else if (angle == 270) {
            point[0] = mCenterX;
            point[1] = mCenterY - radius;
        } else {
            arcAngle = Math.PI * (360 - angle) / 180.0;
            point[0] = (float) (mCenterX + Math.cos(arcAngle) * radius);
            point[1] = (float) (mCenterY - Math.sin(arcAngle) * radius);
        }

        return point;
    }

    /**
     * 相对起始角度计算信用分所对应的角度大小
     * @param value send value
     * @return relative andle based on value
     */
    private float calculateRelativeAngleWithValue(int value) {
        return mSweepAngle * value * 1f / mMax;
    }

    /**
     * 信用分对应信用描述
     * @return Based on credit score return grade
     */
    private String calculateCreditDescription() {
        if (mCreditValue > 700) {
            return "Excellent Credit";
        } else if (mCreditValue > 650) {
            return "Excellent Credit";
        } else if (mCreditValue > 600) {
            return "Good Credit";
        } else if (mCreditValue > 550) {
            return "Average Credit";
        }
        return "Poor Credit";
    }

    /**
     * 信用分对应信用描述
     * @param value range value
     * @return Color background selection based on range
     */
    private int calculateBGColorWithValue(int value) {
        if (value > 700) {
            return mBgColors[4];
        } else if (value > 650) {
            return mBgColors[3];
        } else if (value > 600) {
            return mBgColors[2];
        } else if (value > 550) {
            return mBgColors[1];
        }
        return mBgColors[0];
    }

    private SimpleDateFormat mDateFormat;

    private String getFormatTimeStr() {
        if (mDateFormat == null) {
            mDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
        }
        return String.format("EvalTime:%s", mDateFormat.format(new Date()));
    }

    public int getCreditValue() {
        return mCreditValue;
    }

    /**
     * 设置信用值
     *
     * @param creditValue 信用值
     */
    public void setCreditValue(int creditValue) {
        if (mCreditValue == creditValue || creditValue < mMin || creditValue > mMax) {
            return;
        }

        mCreditValue = creditValue;
        invalidate();
    }
}
